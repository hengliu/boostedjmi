import os
import librosa
import numpy as np

currentdir_index = 20

path = '/home/audio_adversarial_examples/data_speech_commands/'
# There are 35 targets, each is a subfolder name
targets = ['one', 'left', 'on', 'two', 'cat', 'visual', 'up', 'happy', 'zero', 'stop', 'sheila', 'dog', 'bird', 'off', 'seven', 'six', 'house', 'right', 'marvin', 'learn', 'backward', 'yes', 'bed', 'go', 'eight', 'three', 'down', 'follow', 'forward', 'tree', 'nine', 'no', 'four', 'five', 'wow']
currentdir = targets[currentdir_index]

# For a benign audio, we randomly select one adversarial target which doesn't equal to its original label
for audio in os.listdir(path + currentdir):
	try:
		print('current processing audio ' + audio)
		target = targets[currentdir_index]
		while currentdir == target:
			target = targets[np.random.randint(35)]

		output_audio = path + currentdir + '/' + audio[:-4] + 'adver' + '.wav'
		input_audio = path + currentdir + '/' + audio + ' '
		os.system('python3 attack.py --in ' + input_audio + '--target ' + '\'' + target + '\'' + ' --out ' + output_audio)
	except:
		print('corrupted audio: ' + audio)
