#!/usr/bin/env bash 

#The singularity need to start with --nv specification: sudo singularity shell --nv *img

apt-get update
apt-get install ssh

pip3 install --upgrade pip
pip3 install --upgrade tensorflow-gpu==1.8.0
pip3 install deepspeech==0.1.1
pip3 install scipy
pip3 install pandas
pip3 install python_speech_features
pip3 install pyxdg
pip3 install librosa

cd /home
git clone https://github.com/liuheng5712/audio_adversarial_examples.git
cd audio_adversarial_examples
mkdir data_speech_commands
git clone https://github.com/mozilla/DeepSpeech.git
cd DeepSpeech
git checkout tags/v0.1.1
cd ..
wget https://github.com/mozilla/DeepSpeech/releases/download/v0.1.0/deepspeech-0.1.0-models.tar.gz
tar -xzf deepspeech-0.1.0-models.tar.gz
python3 make_checkpoint.py
rm deepspeech-0.1.0-models.tar.gz

#cd models
#git clone https://gitlab.com/hengliu/boostedjmi.git
#mv boostedjmi/sess* ./
#mv boostedjmi/checkpoint ./
#mv boostedjmi/runjobs.py ../
#mv boostedjmi/backward ../data_speech_commands/
